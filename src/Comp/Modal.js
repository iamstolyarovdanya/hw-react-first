function Modal (props){

    return(
        <div id="blur" className="blur" onClick={(e)=>(
                e.target.id === "blur" && props.SetActive()
        )}>
        <div style={{backgroundColor: props.colorCard}} className="modal">
            <h2 style={{backgroundColor: props.colorCardTitle}} className="modal__title">{props.titleName}</h2>
            <p>{props.Modal_text}</p>
            {props.Cross && <span className="Cross_cancel" onClick={props.SetActive} >&times;</span> }
            <button  onClick={props.SetActive}>OK</button>
            <button  onClick={props.SetActive}>Cansel</button>


        </div>
          </div>
    )
}
export default Modal