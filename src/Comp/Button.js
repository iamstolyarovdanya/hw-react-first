function CompButton(props){
    const styleModal = {
        backgroundColor : props.color,
        border: `none`
    }
    return(
        <>
            <button style={styleModal } onClick={props.SetActive}>{props.text}</button>

        </>
    )
}
export default CompButton