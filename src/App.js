import './App.css';
import {useState} from "react";
import CompButton from "./Comp/Button";
import Modal from "./Comp/Modal";

function App() {
  const [isFirstModalOpen , SetIsFirstModalOpen] = useState(false);
    const [isSecondModalOpen , SetIsSecondModalOpen] = useState(false);
  function openFirsModal(){
      SetIsFirstModalOpen(true)
  }
    function closeFirsModal(){
        SetIsFirstModalOpen(false)
    }
    function openSecondModal(){
        SetIsSecondModalOpen(true)
    }
    function closeSecondModal(){
        SetIsSecondModalOpen(false)
    }
  return (
    <div className="App">
        <CompButton
            text= "Delete  "
            SetActive = {openFirsModal}
            color = "green" />
        <CompButton
            text= "Save"
            SetActive = {openSecondModal}
            color = "grey" />
        {isFirstModalOpen &&
         <Modal titleName="Delete this object?"
            SetActive = {closeFirsModal}
             Cross = {true}
             Modal_text="If you want delete this object press ok"
             colorCard = "red"
             colorCardTitle=" rgba(152, 0, 0, 0.68)" /> }
        {isSecondModalOpen &&
         <Modal
             titleName="Save this object ?"
             SetActive = {closeSecondModal}
             Cross ={false}
             Modal_text="If you want save this object, press ok"
             colorCard = "blue"
             colorCardTitle=" rgba(0, 43, 152, 0.68)"/>}
    </div>
  );
}
export default App;